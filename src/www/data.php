<?php

require __DIR__ . "/../vendor/autoload.php";
header('Content-Type: text/html; charset=utf-8');

$matcher = \Atrox\Matcher::multi("//p" , [
	"question" => "./node()" ,
	"answer" => \Atrox\Matcher::multi("./following-sibling::ul[1]/li/div/strong[1]" , [
			"text" => "./node()" ,
		]) ,
	"options" => \Atrox\Matcher::multi("./following-sibling::ul[1]/li/div[not(./strong)]" , [
			"text" => "./node()" ,
		]) ,
])->fromHtml();

$html = file_get_contents(__DIR__ . "/source/questions.html");
$results = $matcher($html);
$output = [];

foreach ($results as $result) {
	if (!isset($result["answer"][0])) {
		continue;
	}

	$question = [];
	$question["text"] = ($result["question"]);
	$question["right"] = ($result["answer"][0]["text"]);
	$anwers = [];
	foreach ($result["answer"] as $ans) {
		$anwers[] = ($ans["text"]);
	}
	foreach ($result["options"] as $ans) {
		$anwers[] = ($ans["text"]);
	}
	$question["options"] = $anwers;
	$output[] = $question;
}

echo json_encode($output);
die();

 