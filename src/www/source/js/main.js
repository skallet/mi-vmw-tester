/**
 * Created by Milan on 29.1.15.
 */
var app = angular.module('vmw', []);

app.controller('QuestionCtrl', [
    '$scope',
    '$http',
    '$timeout',
    function ($scope, $http, $timeout) {

        var $data = undefined;
        var $canAnswer = false;

        $scope.ok = 0;
        $scope.bad = 0;
        $scope.loading = true;
        $scope.show = false;
        $scope.counter = 0;

        var shuffle = function (o){
            for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        };

        var changeQuestion = function () {
            if ($data.length <= $scope.counter) {
                $scope.counter = 0;
            }

            var q = $data[$scope.counter];
            $scope.show = false;
            $scope.question = q;
            $scope.options = shuffle(q.options);
            $scope.counter += 1;
            $canAnswer = true;
        };

        $scope.reset = function () {
            $scope.ok = 0;
            $scope.bad = 0;
            $scope.show = false;
            $scope.counter = 0;
            changeQuestion();
        }

        $scope.select = function (a) {
            if (!$canAnswer) {
                return;
            }
            $canAnswer = false;

            if (a == $scope.question.right) {
                $scope.ok += 1;
                $timeout(changeQuestion, 500);
            } else {
                $scope.bad += 1;
                $timeout(changeQuestion, 3000);
            }

            $scope.selected = a;
            $scope.show = true;
        };

        $scope.random = function() {
            return 0.5 - Math.random();
        }

        $http.get('/data.php')
            .success(function (data) {
                $data = data;
                changeQuestion();
                $scope.loading = false;
            });

    }
]);
