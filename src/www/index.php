<?php

require __DIR__ . "/../vendor/autoload.php";

$params = [];
// @todo: not static, load from url
$params['basePath'] = '';

$latte = new Latte\Engine;
$latte->setTempDirectory(__DIR__ . '/../temp');
$latte->render(__DIR__ . '/../templates/layout.latte' , $params);
 